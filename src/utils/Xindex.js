import { Toast } from "native-base";

class utils {
    showToast(msg = '', duration = 2000) {
        Toast.show({
            text: msg,
            duration: duration,
            position: 'bottom',
            textStyle: { textAlign: "center", color: "#FFF", fontSize: 16 },
            type: "danger",
            style: {
                backgroundColor: "#222530",
                minHeight: 50,
                borderRadius: 5,
                bottom: 0
            }
        })
    }

    validateEmail(str) {
        var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(str);
    }

}

export default new utils();

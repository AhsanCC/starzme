
import React from 'react';
import { Toast } from "native-base";
import { AsyncStorage, RefreshControl, Alert, StatusBar, Platform, } from 'react-native';
class utils {

    showToast(msg = '', duration = 4000) {
        Toast.show({
            text: msg,
            duration: duration,
            position: 'bottom',
            textStyle: { textAlign: "center", color: "#FFF", fontSize: 16 },
            type: "danger",
            style: {
                backgroundColor: "#222530",
                minHeight: 50,
                borderRadius: 5,
                bottom: 0
            }
        })
    }

    validateEmail(str) {
        var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(str);
    }

    isEmpty(obj) {
        for (var key in obj) {
            if (obj.hasOwnProperty(key))
                return false;
        }
        return true;
    }
    isEmptyOrSpaces(str) {
        return str === null || str.match(/^ *$/) !== null;
    }

    refreshControl(refhresList, isRef = false) {
        return (
            <RefreshControl
                refreshing={isRef}
                onRefresh={refhresList}
                title={'Pull to Refresh'}
                tintColor={"#fff"}
                colors={['white']}
                progressBackgroundColor={"#000"}
            />
        )
    }

}

export default new utils();

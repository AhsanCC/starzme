import { Baseurl } from "../components/common";
import utils from "../utils";

class Services {

    async get(endpoint, headers = {}) {
        var postData = {
            method: 'GET',
            headers
        }
        return fetch(Baseurl + endpoint, postData)
            .then(response => response.json())
            .then(response => {

                if (response) {
                    return Promise.resolve(response);
                }
                return Promise.reject(response);
            }).catch(error => {
                error.errorCode = 999;
                return Promise.reject(error);
            });
    }

    async put(endpoint, headers = {}, body = {}, isFormData = false) {
        var postData = {
            method: 'PUT',
            headers
        }

        if (!utils.isEmpty(body)) {
            postData.body = isFormData ? body : JSON.stringify(body)
        }

        return fetch(Baseurl + endpoint, postData)
            .then(response => response.json())
            .then(response => {
                if ('response' in response) {
                    return Promise.resolve(response);
                }
                return Promise.reject(response);
            }).catch(error => {

                return Promise.reject(error);
            })
    }

    async post(endpoint, headers = {}, body = {}, isFormData = false) {
        var postData = {
            method: 'POST',
            headers
        };

        if (!utils.isEmpty(body)) {
            postData.body = isFormData ? body : JSON.stringify(body)
        }

        return fetch(Baseurl + endpoint, postData)
            .then((response) => response.json())
            .then(response => {
                if ('response' in response) {
                    return Promise.resolve(response);
                }
                return Promise.resolve(response);
            }).catch(error => {
                return Promise.reject(error);
            })
    }

    async delete(endpoint, headers = {}, body = {}, isFormData = false) {
        var postData = {
            method: 'DELETE',
            headers,
        }

        if (!utils.isEmpty(body)) {
            postData.body = isFormData ? body : JSON.stringify(body)
        }

        return fetch(Baseurl + endpoint, postData)
            .then(response => response.json())
            .then(response => {
                if ('response' in response) {
                    return Promise.resolve(response);
                }
                return Promise.reject(response);
            }).catch(error => {
                error.errorCode = 999;
                return Promise.reject(error);
            })

    }

    async patch(endpoint, headers = {}, body = {}, isFormData = false) {
        var postData = {
            method: 'PATCH',
            headers
        }

        if (!utils.isEmpty(body)) {
            postData.body = isFormData ? body : JSON.stringify(body)
        }

        return fetch(Baseurl + endpoint, postData)
            .then(response => response.json())
            .then(response => {
                if ('response' in response) {
                    return Promise.resolve(response);
                }
                return Promise.reject(response);
            }).catch(error => {
                error.errorCode = 999;
                return Promise.reject(error);
            });
    }

}
export default new Services();
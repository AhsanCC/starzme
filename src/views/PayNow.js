/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard
} from 'react-native';
import { Header, ButtonBg, PaymentBg, ButtonBg2, Baseurl, Mask } from '../components/common';
import styles,{width} from '../styles/style';
import { NavigationActions, StackActions } from 'react-navigation';
import utils from '../utils';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class PayNow extends Component {
constructor(props) {
        super(props);
        const {state} = props.navigation;
        this.state = {
          headerText:"Membership Request",
          packageData:state.params.package_data,
          user_Data:[],
          loadMask:false
        }
}

static navigationOptions = {
    title: "PayNow",
    header:null
};

componentDidMount(){
    this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')

      if (value !== null){
        var data = JSON.parse(value)
        this.setState({user_Data:data})
        console.log(this.state.user_Data)
        console.log(this.state.packageData)
      }
      else {

      }
    }
    catch (error) {
       console.log(error)
    }
}

submitPayment(){
  this.setState({loadMask:true})
  const payload = {
    nonce : "12345",
    package_id : this.state.packageData.id
  }
  var userToken = this.state.user_Data.access_token
  console.log(payload)
  fetch(Baseurl+'buy/package', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+userToken,
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      this.setState({loadMask:false})
      if ("response" in responseJson) {
        utils.showToast('Thank you for your request. We will get back to you shortly!');
        const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({ routeName: 'Dashboard' })
              ],
        });

        this.props.navigation.dispatch(resetAction);
      }
      else {
        utils.showToast(responseJson.error.messages[0]);
      }

    })
    .catch((error) => {
        this.setState({loadMask:false})
        utils.showToast("A network error occurred!");
    });

}

packageDetail(){
  if(this.state.packageData != null || this.state.packageData != undefined){
    var ac_name = this.state.packageData.bankdetail.account_name
    var ac_num = this.state.packageData.bankdetail.account_number
    var ac_bank = this.state.packageData.bankdetail.bank_name
    var ac_iban = this.state.packageData.bankdetail.iban_number
    return (
      <View style={{flex:1}}>
        <View style={[styles.cardView, {paddingVertical:0, paddingHorizontal:0}]}>
          <ImageBackground style={[styles.cardView, {overflow:"hidden"}]} source={PaymentBg}>
              <Text style={styles.payText}>{this.state.packageData.title}</Text>
              <Text style={styles.payTextDesc}>Duration : {this.state.packageData.duration}</Text>
              <Text style={styles.payTextAmount}>{this.state.packageData.symbol} {this.state.packageData.price}</Text>
          </ImageBackground>
        </View>
        <View style={[styles.cardView, {marginTop:20}]}>
          <Text style={styles.payTextDesc}>If you wish to avail this membership, please confirm below and send your amount in the following account:</Text>
          <Text style={[styles.payTextAmount, {fontSize:14, paddingVertical:5}]}>Account Name: {ac_name}</Text>
          <Text style={[styles.payTextAmount, {fontSize:14, paddingVertical:5}]}>Account Number: {ac_num}</Text>
          <Text style={[styles.payTextAmount, {fontSize:14, paddingVertical:5}]}>Bank Name: {ac_bank}</Text>
          <Text style={[styles.payTextAmount, {fontSize:14, paddingVertical:5}]}>IBAN: {ac_iban}</Text>
          <Text style={styles.payTextDesc}>Procedure: Send proof of payment on this email address <Text style={{color:"#f27f55", fontWeight:"bold"}}>actipasslimitless@gmail.com</Text> and press confirm. You can also send in complaints and suggestions.</Text>
          <ButtonBg onPress={()=> this.submitPayment()} label="CONFIRM" />
          <ButtonBg2 onPress={()=> this.props.navigation.goBack()} label="CANCEL" />
        </View>
      </View>
    )
  }
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
        <View style={[styles.dashboardMainView]}>
          <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
           {this._renderMask()}
            <View style={{flex:1}}>
              <ScrollView showsVerticalScrollIndicator={false} bounces={false} style={styles.marginTopMinus}>
                <View style={{flex:1, paddingHorizontal:20}}>
                    {this.packageDetail()}
                </View>
              </ScrollView>
            </View>
          </View>
    );
  }

}

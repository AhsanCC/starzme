/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  FlatList
} from 'react-native';
import { Header, CameraIcon, ButtonBg, PlaceHolder, Mask, Baseurl } from '../components/common';
import styles,{width} from '../styles/style';
import * as Animatable from 'react-native-animatable';
import { NavigationActions, StackActions } from 'react-navigation';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default class CheckinHistory extends Component {
constructor(props) {
    super(props);
    const {state} = props.navigation;
    this.state = {
      headerText:"Check-ins History",
      user_Data:null,
      gym_id:state.params.gymid,
      checkinHistory:null
    }
}

static navigationOptions = {
    title: "CheckinHistory",
    header:null
};

componentDidMount(){
    this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')
      if (value !== null){
        var data = JSON.parse(value);
        this.setState({
          user_Data:data
        })
        console.log(this.state.user_Data)
        this.getCheckinHistory()
      }
      else {

      }
    }
    catch (error) {
        alert(error)
    }
}

getCheckinHistory(){
      this.setState({loadMask:true})
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"app/checkin/list?gym_id="+this.state.gym_id, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({checkinHistory:responseJson.response.data, loadMask:false})
          }
          else {
              this.setState({checkinHistory:"", loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({checkinHistory:"", loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

_renderCheckinHistory = ({ item, index }) => {
  var animationDelay = index*50
  var time = item.created_time
  var date = item.created_date
  var day = item.created_day

  return (
      <TouchableOpacity activeOpacity={1}>
        <Animatable.View
        animation="slideInUp"
        iterationCount={1}
        style={{flex:1}}
        direction="alternate"
        delay={animationDelay}>
          <View style={[styles.cardView, {marginVertical:10, paddingVertical:15}]}>
              <Text style={styles.checkinText}>{time}, {day} {date}</Text>
          </View>
        </Animatable.View>
      </TouchableOpacity>
  );
}

checkinData(){
  if(this.state.checkinHistory == null){

  }
  else if(this.state.checkinHistory == ""){
    return (
      <PlaceHolder text={"No check-ins found!"} />
    )
  }
  else {
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
        <View style={{flex:1}}>
            <FlatList
            style={{width:"100%", paddingHorizontal:20, flex:1, minHeight:screenHeight/1.5, paddingBottom:20}}
            data={this.state.checkinHistory}
            renderItem={this._renderCheckinHistory} />
        </View>
      </ScrollView>
    )
  }
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
        <View style={[styles.dashboardMainView]}>
          {this._renderMask()}
          <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
            <View style={{flex:1}}>
              {this.checkinData()}
            </View>
          </View>
    );
  }

}

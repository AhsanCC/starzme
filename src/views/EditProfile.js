/* @flow */
import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Keyboard,
  StatusBar,
  FlatList,
  Platform
} from 'react-native';
import { Header, CameraIcon, ButtonBg, Mask, Baseurl } from '../components/common';
import { NavigationActions, StackActions } from 'react-navigation';
import styles,{width} from '../styles/style';
import utils from '../utils';
import ImagePicker from 'react-native-image-picker';
import {ActionSheet} from 'native-base';
import Modal from 'react-native-modalbox';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

const swipeThresholdHeight = 50
const swipeAreaHeight = screenHeight / 3

var BUTTONS = ["Male", "Female", "Cancel"];
var CANCEL_INDEX = 2;

var options = {
  title: 'Upload Picture',
  quality:0.3,
  allowsEditing:true,
  storageOptions: {
    skipBackup: true,
  }
};

export default class EditProfile extends Component {
constructor(props) {
        super(props);
        const {state} = props.navigation;
        this.state = {
          headerText:"Edit Profile",
          loadMask:false,
          user_Data:null,
          user_image:"",
          user_email:"",
          genderText:"Gender",
          user_fname:"",
          user_lname:"",
          user_address:"",
          user_age:"",
          cityText:"City",
          city_id:null,
          imageTaken:false,
          isOpen: false,
          isDisabled: false,
          swipeToClose: true,
          locations:[],
        }
}

static navigationOptions = {
    title: "EditProfile",
    header:null
};

componentDidMount(){
  this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')
      //console.log(udidToken)
      if (value !== null){
        var data = JSON.parse(value);
        this.setState({
          user_Data:data,
          user_image:data.image_urls["1x"],
          user_email:data.email,
          genderText:data.gender,
          user_fname:data.first_name,
          user_lname:data.last_name,
          user_address:data.address,
          user_age:data.age,
          city_id:data.city_id,
          cityText:data.city_name
        })
        console.log(this.state.user_Data)
      }
      else {

      }
    }
    catch (error) {
        alert(error)
    }
}

_onSubmitEditing = (cur, next) => {
    if (next != null) {
      this.refs[next].focus()

    } else {
      this.refs[cur].blur()
    }
}

openCameraOptions(type){
  ImagePicker.showImagePicker(options, (response) => {
  console.log('Response = ', response);

  if (response.didCancel) {
    console.log('User cancelled image picker');
  }
  else if (response.error) {
    console.log('ImagePicker Error: ', response.error);
  }
  else if (response.customButton) {
    console.log('User tapped custom button: ', response.customButton);
  }
  else {

    let source = response.uri;

      this.setState({
        imageTaken:true,
        user_image:source
      });
  }
});
}

submitUpdateProfile(){
   if(this.state.user_fname == undefined || this.state.user_fname == "") {
      utils.showToast('Please enter your first name!');
   }
   else if(this.state.user_lname == undefined || this.state.user_lname == "") {
      utils.showToast('Please enter your last name!');
   }
   else if(this.state.user_age == undefined || this.state.user_age == ""){
      utils.showToast('Please enter your age!');
   }
   else {
     var _body = new FormData();
     _body.append('first_name', this.state.user_fname);
     _body.append('last_name', this.state.user_lname);
     _body.append('gender', this.state.genderText);
     _body.append('age', this.state.user_age);
     _body.append('city_id', this.state.city_id);

     if(this.state.imageTaken == true){
         _body.append('image', {
           uri: this.state.user_image,
           name: 'file',
           type: 'image/jpeg',
         })
     }

         console.log(_body)
         var userToken = this.state.user_Data.access_token
         this.setState({loadMask:true});
           fetch(Baseurl+'profile/update', {
               method: 'POST',
               headers: {
                 'Authorization': 'Bearer ' + userToken,
                 'Content-Type': 'multipart/form-data'
               },
               body: _body,
             })
             .then((response) => response.json())
             .then((responseJson) => {
               console.log(responseJson)

               if ("response" in responseJson) {
                 var data = responseJson.response.data
                 this.saveData(data)
                 utils.showToast('Your profile has been udpated!');
               }
               else {
                 this.setState({loadMask:false});
                 utils.showToast(responseJson.error.messages[0]);
               }

             })
             .catch((error) => {
               this.setState({loadMask:false});
               utils.showToast("A network error occurred!");
             });
    }

}

async saveData(userData) {
    try {
      var data = JSON.stringify(userData);
      await AsyncStorage.setItem('userData', data)
      this.setState({ loadMask: false });
      setTimeout(() => {

        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Dashboard' })
          ],
        });

        this.props.navigation.dispatch(resetAction);

      }, 300)

    }
    catch (error) {
      console.log(error)
    }
}

openActionsheet(){
  ActionSheet.show (
    {
      options:BUTTONS,
      cancelButtonIndex: CANCEL_INDEX,
      title: "Gender"
    },
    buttonIndex => {

      if(buttonIndex != 2){
          this.setState({ genderText: BUTTONS[buttonIndex]});
      }

    }
 )
}

getCities(){
      this.setState({ loadMask: true })
      this.openModal()
      fetch(Baseurl+"city/list", {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({locations:responseJson.response.data, loadMask:false})
          }
          else {
              this.setState({locations:[], loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({locations:[], loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

openModal() {
  this.refs.modal1.open()
}

close_Modal() {
  this.refs.modal1.close()
  this.setState({ isOpen: false })
}

_onLocationSelect = (id, name) => {
  this.setState({
    city_id: id,
    cityText: name
  })
  this.refs.modal1.close()
}

_renderLocations = ({ item, index }) => {
  var locationTitle = item.name
  return (
    <TouchableOpacity onPress={() => this._onLocationSelect(item.id, item.name)}>
      <View style={{ flex: 1, borderWidth: 0.2, borderColor: "#8a93a5", borderRadius: 5, marginVertical: 5, marginHorizontal: 10, paddingHorizontal:10, backgroundColor:"#FFF" }}>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1, paddingVertical: 5, flexDirection: "column" }}>
            <Text numberOfLines={1} style={[styles.fontBook, { color: "#000", fontSize: 16, paddingLeft: 5, paddingVertical: 10 }]}>{locationTitle}</Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

modalHeader() {
  return (
    <Header leftIcon="back" headerText="Select City" onLeftPress={()=> this.close_Modal()} onRightPress={null} />
  )
}

renderModal() {
  return (
    <Modal
      style={[styles.modal, styles.modal1]}
      ref={"modal1"}
      swipeToClose={this.state.swipeToClose}
      onClosed={this.onClose}
      onOpened={this.onOpen}
      animationDuration={500}
      swipeArea={swipeAreaHeight}
      onClosingState={this.onClosingState}>

      <View style={{ flex: 1 }}>
        <StatusBar barStyle="light-content" />
        {this.modalHeader()}
        <View style={[styles.marginTopMinus, { flex: 1 }]}>
          <View style={{ flex: 1 }}>
            <FlatList
              data={this.state.locations}
              renderItem={this._renderLocations} />
          </View>
        </View>
      </View>

    </Modal>
  )
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    if(this.state.user_Data != null){
      if(this.state.user_image == ""){
        var userImage = "https://www.w3schools.com/howto/img_avatar.png"
      }
      else {
        var userImage = this.state.user_image
      }
    }
    return (
      <KeyboardAvoidingView behavior={Platform.OS == 'ios' ? "padding" : null} style={{flex:1, backgroundColor:"#FFF"}}>
        <View style={[styles.dashboardMainView]}>
         {this._renderMask()}
         {this.renderModal()}
          <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
            <View style={{flex:1}}>
              <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
                <View style={{flex:1, paddingHorizontal:20}}>
                    <View style={styles.cardView}>
                      <View style={{flex:1, alignItems:"center", justifyContent:"center"}}>
                        <TouchableOpacity onPress={()=> this.openCameraOptions()}>
                          <View style={styles.editImageCont}>
                              <Image source={{uri:userImage}} style={styles.editImage} />
                              <View style={styles.cameraIconCont}>
                                <Image source={CameraIcon} style={styles.cameraIconImage} />
                              </View>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={{flex:1}}>
                        <View style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>First Name</Text>
                              <TextInput
                              style={styles.formInputInner}
                              placeholderTextColor={"#7b8799"}
                              underlineColorAndroid="transparent"
                              ref={'fname'}
                              value={this.state.user_fname}
                              returnKeyType="next"
                              onSubmitEditing={() => this._onSubmitEditing('fname', 'lname')}
                              onChangeText={(text) => this.setState({user_fname:text})}
                              placeholder="Enter first name" />
                          </View>
                        </View>
                        <View style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>Last Name</Text>
                              <TextInput
                              style={styles.formInputInner}
                              placeholderTextColor={"#7b8799"}
                              underlineColorAndroid="transparent"
                              value={this.state.user_lname}
                              ref={'lname'}
                              returnKeyType="next"
                              onSubmitEditing={() => this._onSubmitEditing('lname', null)}
                              onChangeText={(text) => this.setState({user_lname:text})}
                              placeholder="Enter last name" />
                          </View>
                        </View>
                        <TouchableOpacity onPress={()=> this.openActionsheet()} style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>Gender</Text>
                              <Text style={styles.formInputInnerText}>{this.state.genderText}</Text>
                          </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> this.getCities()} style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>City</Text>
                              <Text style={styles.formInputInnerText}>{this.state.cityText}</Text>
                          </View>
                        </TouchableOpacity>
                        <View style={styles.labelContainerInner}>
                          <View style={{flex:1}}>
                              <Text style={styles.labelFormInner}>Age</Text>
                              <TextInput
                              style={styles.formInputInner}
                              placeholderTextColor={"#7b8799"}
                              underlineColorAndroid="transparent"
                              value={this.state.user_age}
                              maxLength={3}
                              returnKeyType="done"
                              keyboardType="number-pad"
                              onChangeText={(text) => this.setState({user_age:text})}
                              placeholder="Enter age" />
                          </View>
                        </View>
                      </View>
                      <ButtonBg onPress={()=> this.submitUpdateProfile()} label="SUBMIT" />
                    </View>
                </View>
              </ScrollView>
            </View>
          </View>
      </KeyboardAvoidingView>
    );
  }

}

/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
} from 'react-native';
import styles from '../styles/style';
import { NavigationActions, StackActions } from 'react-navigation';
import { Baseurl, Mask, Header, NotificationIcon, ButtonBg, PlaceHolder } from '../components/common';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import utils from '../utils';

export default class Rating extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"Rate this gym",
        rating:0,
        experience:"Rate your experience!",
        user_Data:null,
        gymDetail:null,
        gymId:state.params.gym_id,
        loadMask:true,
        notId:state.params.not_id
      }
}

componentDidMount(){
    this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')

      if (value !== null){
        var data = JSON.parse(value)
        this.setState({user_Data:data})
        this.getGymDetail()
      }
      else {

      }
    }
    catch (error) {
       console.log(error)
    }
}

getGymDetail(){
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"app/gym/detail?id="+this.state.gymId, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({gymDetail:responseJson.response.data, loadMask:false})
          }
          else {
              this.setState({gymDetail:"", loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({gymDetail:"", loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

addRating(){
  const payload = {
       gym_id : this.state.gymId,
       rating: this.state.rating,
       notification_id:this.state.notId
  }

  if(this.state.rating == 0){
    utils.showToast('Please rate your experience!');
  }
  else {
    this.setState({loadMask:true})
    var userToken = this.state.user_Data.access_token
    console.log(payload)
    fetch(Baseurl+'app/manage/rating/gym', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+userToken,
        },
        body: JSON.stringify(payload),
      })
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if ("response" in responseJson) {
          utils.showToast('You have successfully rated this gym!');
          const resetAction = StackActions.reset({
                index: 0,
                actions: [
                  NavigationActions.navigate({ routeName: 'Dashboard' })
                ],
          });

          this.props.navigation.dispatch(resetAction);
        }
        else {
          this.setState({loadMask:false})
          utils.showToast(responseJson.error.messages[0]);
        }

      })
      .catch((error) => {
          this.setState({loadMask:false})
          utils.showToast("A network error occurred!");
      });
  }

}

changeRatingState(rat){
  if(rat == 1){
    this.setState({rating:rat, experience:"Very Poor"})
  }
  else if(rat == 2){
    this.setState({rating:rat, experience:"Poor"})
  }
  else if(rat == 3){
    this.setState({rating:rat, experience:"Satisfactory"})
  }
  else if(rat == 4){
    this.setState({rating:rat, experience:"Good"})
  }
  else if(rat == 5){
    this.setState({rating:rat, experience:"Excellent"})
  }
}

ratingButtons(){
  var rateReview = this.state.rating
  if(rateReview == 0){
    return (
      <View style={{flexDirection:"row", justifyContent:"center", marginVertical:5}}>
          <TouchableOpacity onPress={()=> this.changeRatingState(1)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(2)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(3)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(4)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(5)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
      </View>
    )
  }
  else if(rateReview == 1){
    return (
      <View style={{flexDirection:"row", justifyContent:"center", marginVertical:5}}>
          <TouchableOpacity onPress={()=> this.changeRatingState(1)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(2)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(3)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(4)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(5)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
      </View>
    )
  }
  else if(rateReview == 2){
    return (
      <View style={{flexDirection:"row", justifyContent:"center", marginVertical:5}}>
          <TouchableOpacity onPress={()=> this.changeRatingState(1)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(2)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(3)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(4)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(5)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
      </View>
    )
  }
  else if(rateReview == 3){
    return (
      <View style={{flexDirection:"row", justifyContent:"center", marginVertical:5}}>
          <TouchableOpacity onPress={()=> this.changeRatingState(1)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(2)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(3)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(4)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(5)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
      </View>
    )
  }
  else if(rateReview == 4){
    return (
      <View style={{flexDirection:"row", justifyContent:"center", marginVertical:5}}>
          <TouchableOpacity onPress={()=> this.changeRatingState(1)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(2)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(3)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(4)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(5)}><Icon style={styles.starIconsBig} name="star-outline" /></TouchableOpacity>
      </View>
    )
  }
  else if(rateReview == 5){
    return (
      <View style={{flexDirection:"row", justifyContent:"center", marginVertical:5}}>
          <TouchableOpacity onPress={()=> this.changeRatingState(1)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(2)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(3)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(4)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
          <TouchableOpacity onPress={()=> this.changeRatingState(5)}><Icon style={styles.starIconsBig} name="star" /></TouchableOpacity>
      </View>
    )
  }
}

ratingData(){
  if(this.state.gymDetail == null){

  }
  else if(this.state.gymDetail == ""){
    return (
        <PlaceHolder text={"Network error occurred!"} />
    )
  }
  else {
    var gymD = this.state.gymDetail
    var gymLogo = gymD.image_urls["2x"]
    var gymName = gymD.title
    return (
      <View style={[styles.marginTopMinus, {flex:1}]}>
        <View style={{flex:1, paddingHorizontal:20}}>
            <View style={styles.cardView}>
            <View style={{flex:1, paddingHorizontal:10, paddingBottom:10, paddingTop:20}}>
                  <View style={{alignItems:"center", justifyContent:"center", marginVertical:20, flex:1}}>
                    <View style={{width:100, height:100, borderRadius:50, borderWidth:5, borderColor:"#e6eaeb", alignItems:"center", justifyContent:"center"}}>
                        <View style={{width:90, height:90, overflow:"hidden", borderRadius:55}}>
                          <Image
                          resizeMode='cover'
                          style={{width:null, height:90}}
                          source={{uri:gymLogo}} />
                        </View>
                    </View>
                    <View style={{flex:1, paddingHorizontal:15, paddingVertical:5, flexDirection:"column"}}>
                        <Text numberOfLines={1} style={[styles.fontBook, {color:"#000", fontSize:20, fontWeight:"bold", paddingVertical:5, textAlign:"center", fontFamily:"Montserrat-Regular"}]}>{gymName}</Text>
                   </View>
                  </View>
                  <View style={{flex:0.5}}>
                      {this.ratingButtons()}
                      <Text style={{color:"#f3b118", fontSize:14, fontWeight:"bold", paddingVertical:5, textAlign:"center", fontFamily:"Montserrat-Regular"}}>{this.state.experience}</Text>
                  </View>
                  <ButtonBg onPress={()=> this.addRating()} label="SUBMIT" />
            </View>
            </View>
            <View style={{flex:0.2}}></View>
        </View>
      </View>
    )
  }
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

render() {
    return (
      <View style={[styles.dashboardMainView]}>
        <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.props.navigation.goBack()} onRightPress={null} />
        {this._renderMask()}
        <View style={styles.dashboardContainerView}>
            {this.ratingData()}
        </View>
      </View>
    );
  }
}

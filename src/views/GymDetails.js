/* @flow */
import React, { Component } from 'react';
import {
  View,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
  Text,
  ImageBackground,
  Dimensions,
  ActivityIndicator,
  AsyncStorage,
  TouchableOpacity,
  FlatList,
  StatusBar,
  Linking
} from 'react-native';
import styles from '../styles/style';
import { NavigationActions } from 'react-navigation';
import {
   Baseurl,
   Mask,
   Header,
   ButtonBg,
   LocationIcon,
   MapIcon,
   CallIcon,
   GalleryIcon,
   SaveIcon,
   SaveIconInActive,
   PlaceHolder,
   PlaceHolderSmall,
   ButtonSmall,
   Rating
} from '../components/common';
import {Toast, Icon, ActionSheet} from 'native-base';
const {width, height} = Dimensions.get("window");
import utils from '../utils'

export default class GymDetails extends Component {
constructor(props) {
      super(props);
      const {state} = props.navigation;
      this.state = {
        headerText:"Details",
        selected:1,
        user_Data:null,
        gymDetail:null,
        gymId:state.params.gym_id,
        loadMask:true,
        services:null,
        classes:null
      }
}

componentDidMount(){
   this.getData()
}

async getData(){
    try {
      const value = await AsyncStorage.getItem('userData')

      if (value !== null){
        var data = JSON.parse(value)
        this.setState({user_Data:data})
        this.getGymDetail()
      }
      else {

      }
    }
    catch (error) {
       console.log(error)
    }
}

getGymDetail(){
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"app/gym/detail?id="+this.state.gymId, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({gymDetail:responseJson.response.data, loadMask:false})
              this.getServices()
          }
          else {
              this.setState({gymDetail:"", loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({gymDetail:"", loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

getServices(){
      this.setState({loadMask:true})
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"service/list?gym_id="+this.state.gymId, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({services:responseJson.response.data, loadMask:false})
          }
          else {
              this.setState({services:"", loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({services:"", loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

getClasses(){
      this.setState({loadMask:true})
      var userToken = this.state.user_Data.access_token
      fetch(Baseurl+"class/list?gym_id="+this.state.gymId, {
          method: 'GET',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+userToken,
          },
        })
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
          if("response" in responseJson){
              this.setState({classes:responseJson.response.data, loadMask:false})
          }
          else {
              this.setState({classes:"", loadMask:false})
              utils.showToast(responseJson.error.messages[0]);
          }
        })
        .catch((error) => {
              this.setState({classes:"", loadMask:false})
              utils.showToast("A network error occurred!");
        });
}

addCheckin(){
  this.setState({loadMask:true})
  const payload = {
       gym_id : this.state.gymId
  }
  var userToken = this.state.user_Data.access_token
  console.log(payload)
  fetch(Baseurl+'app/add/checkin', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+userToken,
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      this.setState({loadMask:false})
      if ("response" in responseJson) {
        utils.showToast("You have successfully checked-in!");
      }
      else {
        utils.showToast(responseJson.error.messages[0]);
      }

    })
    .catch((error) => {
        this.setState({loadMask:false})
        utils.showToast("A network error occurred!");
    });

}

markAsFav(status,uid){
  this.setState({loadMask:true})
  const payload = {
       gym_id : uid,
	     action : status
  }
  var userToken = this.state.user_Data.access_token
  console.log(payload)
  fetch(Baseurl+'app/manage/favourite/gym', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+userToken,
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)
      setTimeout(()=>{
        this.getGymDetail()
      },500)

      if ("response" in responseJson) {

      }
      else {
        this.setState({loadMask:false})
        utils.showToast(responseJson.error.messages[0]);
      }

    })
    .catch((error) => {
        this.setState({loadMask:false})
        utils.showToast("A network error occurred!");
    });

}

joinClass(id){
  this.setState({loadMask:true})
  const payload = {
       gymclass_id : id,
       action:"add"
  }
  var userToken = this.state.user_Data.access_token
  console.log(payload)
  fetch(Baseurl+'app/manage/join/class', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+userToken,
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)

      if ("response" in responseJson) {
        utils.showToast('Joined successfully!');
        setTimeout(()=>{
          this.getClasses()
        },500)
      }
      else {
        this.setState({loadMask:false})
        utils.showToast(responseJson.error.messages[0]);
      }

    })
    .catch((error) => {
        this.setState({loadMask:false})
        utils.showToast("A network error occurred!");
    });
}

handlePressCall(ur) {
    var url = "tel:"+ur
    //alert(url)
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Cant handle url: ' + url)
      } else {
        return Linking.openURL(url)
      }
    }).catch(err => console.error('An error occurred', err))
}

submitBooking(){
  this.setState({loadMask:true})
  const payload = {
       gym_id : this.state.gymId
  }
  var userToken = this.state.user_Data.access_token
  console.log(payload)
  fetch(Baseurl+'app/add/booking', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+userToken,
      },
      body: JSON.stringify(payload),
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson)

      if ("response" in responseJson) {
        utils.showToast('Booking successful!');
        setTimeout(()=>{
          this.getGymDetail()
        },500)
      }
      else {
        this.setState({loadMask:false})
        utils.showToast(responseJson.error.messages[0]);
      }

    })
    .catch((error) => {
        this.setState({loadMask:false})
        utils.showToast("A network error occurred!");
    });
}

changeCategoryState(type){
     if (type == 1) {
      this.setState({selected:1})
      this.getServices()
     }
     else if (type == 2) {
      this.setState({selected:2})
      this.getClasses()
     }
}

showSelected(type){
  if (type == this.state.selected) {
    return styles.barHeaderTextSelected
  }
}

showSelected2(type){
  if (type == this.state.selected) {
    return styles.homeHeading
  }
}

otherHeader(){
    return (
       <View style={{width:"100%", flexDirection: "row"}}>
         <View style={{flexDirection:"row", overflow:"hidden", borderRadius:5, width:"100%"}}>
           <TouchableOpacity onPress = {()=> this.changeCategoryState(1)}>
             <View style={[styles.barHeaderTextUnselected, this.showSelected(1)]}>
                  <Text style={[styles.homeHeadingUnSelected, this.showSelected2(1)]}>Services</Text>
             </View>
           </TouchableOpacity>
           <TouchableOpacity style={{flex:1}} onPress = {()=> this.changeCategoryState(2)}>
           <View style={[styles.barHeaderTextUnselected, this.showSelected(2), {flex:1, paddingLeft:20}]}>
                <Text style={[styles.homeHeadingUnSelected, this.showSelected2(2)]}>Classes</Text>
           </View>
           </TouchableOpacity>
          </View>
      </View>
    )
}

_renderServices = ({ item, index }) => {
  var gymImage = item.image_urls["1x"]
  var gymName = item.title
  var gymDesc = item.description
  var duration = item.duration

  return (
      <TouchableOpacity activeOpacity={1}>
        <View style={styles.gymItemContainer}>
            <View style={{flex:1, flexDirection:"row"}}>
              <View style={{flex:1, paddingVertical:5, paddingHorizontal:10}}>
                <Text numberOfLines={1} style={styles.gymListHeading}>{gymName}</Text>
                <Text style={styles.gymListDesc}>{gymDesc}</Text>
                <View style={{marginVertical:5, flexDirection:"column", flex:1}}>
                  <Text numberOfLines={1} style={[styles.gymListHeading, {paddingTop:6, fontSize:13}]}>Duration:</Text>
                  <Text numberOfLines={1} style={[styles.gymListDesc, {fontSize:13}]}>{duration} hours</Text>
                </View>
              </View>
              <View style={styles.gymImageCont}>
                  <View style={styles.gymImageContInner}>
                      <Image source={{uri:gymImage}} style={styles.gymImage} />
                  </View>
              </View>
            </View>
        </View>
      </TouchableOpacity>
  );
}

_renderClasses = ({ item, index }) => {
  var gymImage = item.image_urls["1x"]
  var gymName = item.title
  var gymDesc = item.description
  var duration = item.duration

  if(item.is_joined == 0){
    var joinButton = <View style={{marginVertical:5, marginRight:10}}><ButtonSmall onPress={()=> this.joinClass(item.id)} label="Join now" /></View>
  }
  else {
    var joinButton = <View/>
  }

  return (
      <TouchableOpacity activeOpacity={1}>
        <View style={styles.gymItemContainer}>
            <View style={{flex:1, flexDirection:"row"}}>
              <View style={{flex:1, paddingVertical:5, paddingHorizontal:10}}>
                <Text numberOfLines={1} style={styles.gymListHeading}>{gymName}</Text>
                <Text style={styles.gymListDesc}>{gymDesc}</Text>
                <View style={{flexDirection:"row"}}>
                    {joinButton}
                    <View style={{marginVertical:5, flexDirection:"column", flex:1}}>
                      <Text numberOfLines={1} style={[styles.gymListHeading, {paddingTop:6, fontSize:13}]}>Duration:</Text>
                      <Text numberOfLines={1} style={[styles.gymListDesc, {fontSize:13}]}>{duration} hours</Text>
                    </View>
                </View>
              </View>
              <View style={styles.gymImageCont}>
                  <View style={styles.gymImageContInner}>
                      <Image source={{uri:gymImage}} style={styles.gymImage} />
                  </View>
              </View>
            </View>
        </View>
      </TouchableOpacity>
  );
}

getServicesData(){
  if(this.state.selected == 1){
    if(this.state.services == null){

    }
    else if(this.state.services == ""){
      return (
        <View style={{flex:1, paddingVertical:50}}>
          <PlaceHolderSmall text={"No services found!"} />
        </View>
      )
    }
    else {
      return (
        <View style={{flex:1}}>
            <FlatList
            style={{width:"100%", paddingHorizontal:20, flex:1}}
            data={this.state.services}
            extraData={this.state}
            renderItem={this._renderServices} />
        </View>
      )
    }
  }
  else {
    if(this.state.classes == null){

    }
    else if(this.state.classes == ""){
      return (
        <View style={{flex:1, paddingVertical:50}}>
          <PlaceHolderSmall text={"No classes found!"} />
        </View>
      )
    }
    else {
      return (
        <View style={{flex:1}}>
            <FlatList
            style={{width:"100%", paddingHorizontal:20, flex:1}}
            data={this.state.classes}
            extraData={this.state}
            renderItem={this._renderClasses} />
        </View>
      )
    }
  }
}

renderGymDetail(){
  if(this.state.gymDetail == null){

  }
  else if(this.state.gymDetail == ""){
    return (
        <PlaceHolder text={"Network error occurred!"} />
    )
  }
  else {
    var gymD = this.state.gymDetail
    var gymPic = gymD.cover_image_urls["2x"]
    var gymLogo = gymD.image_urls["1x"]
    var gymName = gymD.title
    var gymGender = gymD.gender
    var gymAdd = gymD.address
    var oh = gymD.open_hours
    var ch = gymD.close_hours
    var gymDesc = gymD.description

    if(gymD.is_favourite == 1){
        var status = "remove"
        var favButton = SaveIcon
    }
    else {
        var status = "add"
        var favButton = SaveIconInActive
    }

    if(gymD.is_booked == 0){
      var bookNowButton = <ButtonBg onPress={()=> this.submitBooking()} label="Book now" />
    }
    else {
      var bookNowButton = <View/>
    }

    return (
      <ScrollView showsVerticalScrollIndicator={false} style={styles.marginTopMinus}>
        <View style={{flex:1, paddingHorizontal:20}}>
            <View style={styles.detailBannerMainCont}>
              <View style={styles.detailBannerCont}>
                <Image source={{uri:gymPic}} style={styles.detailBannerImage}/>
              </View>
            </View>
        </View>
            <View style={{flexDirection:"row", flex:1}}>
              <View style={{flex:0.2}}></View>
              <View style={styles.detailBannerInner}>
                  <View style={styles.bannerDetailIconCont}>
                      <View style={styles.bannerDetailIconContInner}>
                          <Image source={{uri:gymLogo}} style={styles.bannerDetailIconImage} />
                      </View>
                  </View>
                  <View style={{flex:1, paddingHorizontal:10}}>
                      <Text numberOfLines={1} style={styles.gymHeading}>{gymName}</Text>
                      <View style={{flexDirection:"row"}}>
                        <Icon type="Feather" style={{fontSize:12, color:"#73879b", paddingVertical:7, paddingRight:5}} name="map-pin" />
                        <Text numberOfLines={2} style={[styles.homeInnerText, {fontSize:12}]}>{gymAdd}</Text>
                      </View>
                      <View style={{width:"100%", top:-5, paddingLeft:3}}>
                        <Rating rateReview={gymD.rating}/>
                      </View>
                      <View style={{flexDirection:"row"}}>
                        <View style={styles.detailInnerIcons}>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate("GoogleMap", {gymLat:gymD.latitude, gymLong:gymD.longitude})}>
                                <Image source={MapIcon} resizeMode="stretch" style={styles.detailInnerIconsImage} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.detailInnerIcons}>
                            <TouchableOpacity onPress={()=> this.handlePressCall(gymD.contact)}>
                                <Image source={CallIcon} resizeMode="stretch" style={styles.detailInnerIconsImage} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.detailInnerIcons}>
                            <TouchableOpacity onPress={()=> this.props.navigation.navigate("GymGallery", {gym_id:this.state.gymId})}>
                                <Image source={GalleryIcon} resizeMode="stretch" style={styles.detailInnerIconsImage} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.detailInnerIcons}>
                            <TouchableOpacity onPress={()=> this.markAsFav(status,gymD.id)}>
                                <Image source={favButton} resizeMode="stretch" style={styles.detailInnerIconsImage} />
                            </TouchableOpacity>
                        </View>
                      </View>
                  </View>
                  <View style={styles.bannerDetailIconCont}>
                  <View style={styles.bannerDetailIconContInner2}>
                      <TouchableOpacity onPress={()=> this.addCheckin()}>
                        <Image source={LocationIcon} resizeMode="contain" style={styles.bannerDetailIconImage} />
                      </TouchableOpacity>
                  </View>
                  </View>
              </View>
            </View>

       <View style={{flex:1, paddingHorizontal:20}}>
            <View style={{flex:1, paddingVertical:20}}>
                <Text style={styles.homeHeading}>{gymName} <Text style={styles.homeInnerText}>({gymGender})</Text></Text>
                <View style={{flexDirection:"row"}}>
                  <Icon type="Feather" style={{fontSize:12, color:"#73879b", paddingVertical:7, paddingRight:5}} name="clock" />
                  <Text style={[styles.homeInnerText, {fontSize:12}]}>Opening hours - {oh} - {ch}</Text>
                </View>
                <Text style={[styles.homeInnerText, {lineHeight:20}]}>{gymDesc}</Text>
            </View>
            {this.otherHeader()}
      </View>
        <View style={{flex:1, paddingBottom:20}}>
            {this.getServicesData()}
            {bookNowButton}
        </View>
      </ScrollView>
    )
  }
}

_renderMask(){
    if(this.state.loadMask == true){
        return (
          <Mask />
        )
    }
}

backGym(){
  if(this.props.navigation.state.params.onGoBack){
    this.props.navigation.state.params.onGoBack()
    this.props.navigation.goBack()
  }
  else {
    this.props.navigation.goBack()
  }
}

render() {
    return (
      <View style={[styles.dashboardMainView]}>
        <Header leftIcon="back" headerText={this.state.headerText} onLeftPress={()=> this.backGym()} onRightPress={null} />
        {this._renderMask()}
        <View style={styles.dashboardContainerView}>
          {this.renderGymDetail()}
        </View>
      </View>
    );
  }
}

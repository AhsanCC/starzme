/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
  Image,
  TextInput
} from 'react-native';

import { Icon } from 'native-base';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import * as Animatable from 'react-native-animatable';

class UserVideoModal extends Component {
  render(){
    const {
      videoModalAnimation,
      onClose,
      onLikePress,
      onRatePress,
      onFlashPress,
      onSharePress,
      data
    } = this.props;
    console.log(data, ' data')
    return (
      <View style={{width:screenWidth, height:screenHeight, position:"absolute", zIndex:999, backgroundColor: "rgba(0, 0, 0, 0.7)"}}>
        <Animatable.View
        animation={videoModalAnimation}
        style={{width:"100%", height:screenHeight, backgroundColor:"#000"}}
        duration={500}>
            <ImageBackground style={{width:"100%", height:screenHeight}} resizeMode="cover" source={{uri:"https://i.guim.co.uk/img/media/dfd56d28e5b8546c9f2cac7362d03256378cd81a/253_47_1880_2348/master/1880.jpg?width=700&quality=85&auto=format&fit=max&s=ad00c490c7ed5c63435657d811e2bc18"}}>
                <View style={{flex:1, backgroundColor: "rgba(0, 0, 0, 0.3)"}}>
                  <View style={{flex:1, paddingTop:30}}>
                    <View style={{flex:1, flexDirection:"row"}}>
                      <View style={{flex:1}}></View>
                      <View style={{flex:1, alignItems:"flex-end"}}>
                          <TouchableOpacity onPress={onClose} style={{width:50, height:50, alignItems:"center", justifyContent:"center"}}>
                            <Text style={{textAlign:"center", fontSize:30, color:"#FFF"}}>X</Text>
                          </TouchableOpacity>
                      </View>
                    </View>
                      <View style={{flex:1, flexDirection:"row", paddingHorizontal:15}}>
                        <View style={{flex:1}}>
                          <TouchableOpacity activeOpacity={1}>
                            <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                                <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../../assets/UserProfileIcon.png")} />
                            </View>
                            <View style={{width:50, backgroundColor:"#fd3a40", marginVertical:5}}>
                                <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:5}}>LIVE</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <View style={{flex:1, alignItems:"flex-end", justifyContent:"center"}}>
                          <TouchableOpacity>
                          <View style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40, alignItems: "center", justifyContent:"center"}}>
                              <Image style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40, resizeMode:"contain"}} source={require("../../assets/statsIcon.png")} />
                          </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{flex:1, flexDirection:"row", paddingHorizontal:15}}>
                        <View style={{flex:1}}></View>
                        <View style={{flex:1, alignItems:"flex-end", justifyContent:"center"}}>
                          <TouchableOpacity onPress={onRatePress}>
                          <View style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40}}>
                              <Image style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40, resizeMode:"contain"}} source={require("../../assets/ratingIcon.png")} />
                          </View>
                          <View style={{width:screenHeight >= 700 ? 50 : 40}}>
                              <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:2}}>120</Text>
                          </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{flex:1, flexDirection:"row", paddingHorizontal:15}}>
                        <View style={{flex:1}}></View>
                        <View style={{flex:1, alignItems:"flex-end", justifyContent:"center"}}>
                          <TouchableOpacity onPress={onFlashPress}>
                          <View style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40}}>
                              <Image style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40, resizeMode:"contain"}} source={require("../../assets/flashIcon.png")} />
                          </View>
                          <View style={{width:screenHeight >= 700 ? 50 : 40}}>
                              <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:2}}>22</Text>
                          </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{flex:1, flexDirection:"row", paddingHorizontal:15}}>
                        <View style={{flex:1}}></View>
                        <View style={{flex:1, alignItems:"flex-end", justifyContent:"center"}}>
                          <TouchableOpacity onPress={onLikePress}>
                          <View style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40}}>
                              <Image style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40, resizeMode:"contain"}} source={require("../../assets/loveIcon.png")} />
                          </View>
                          <View style={{width:screenHeight >= 700 ? 50 : 40}}>
                              <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:2}}>430</Text>
                          </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{flex:1, flexDirection:"row", paddingHorizontal:15}}>
                        <View style={{flex:1}}></View>
                        <View style={{flex:1, alignItems:"flex-end", justifyContent:"center"}}>
                          <TouchableOpacity>
                          <View style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40}}>
                              <Image style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40, resizeMode:"contain"}} source={require("../../assets/commentIcon.png")} />
                          </View>
                          <View style={{width:screenHeight >= 700 ? 50 : 40}}>
                              <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:2}}>94</Text>
                          </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{flex:1, flexDirection:"row", paddingHorizontal:15}}>
                        <View style={{flex:1}}></View>
                        <View style={{flex:1, alignItems:"flex-end", justifyContent:"center"}}>
                          <TouchableOpacity onPress={onSharePress}>
                          <View style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40}}>
                              <Image style={{width:screenHeight >= 700 ? 50 : 40, height:screenHeight >= 700 ? 50 : 40, resizeMode:"contain"}} source={require("../../assets/shareIcon.png")} />
                          </View>
                          <View style={{width:screenHeight >= 700 ? 50 : 40}}>
                              <Text style={{textAlign:"center", color:"#FFF", fontSize:12, paddingVertical:2}}>29</Text>
                          </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{flex:1, flexDirection:"row", paddingHorizontal:15}}>
                        <View style={{flex:1}}></View>
                        <View style={{flex:1, alignItems:"flex-end", justifyContent:"center"}}>
                          <TouchableOpacity activeOpacity={1}>
                          <View style={{width:50, height:50, borderRadius:25, borderWidth:1, borderColor:"#fd3a40", alignItems: "center", justifyContent:"center"}}>
                              <Image style={{width:40, height:40, resizeMode:"stretch", borderRadius:20}} source={require("../../assets/profileIcon.png")} />
                          </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                      <View style={{alignItems:"center", justifyContent:"center", width:"100%", paddingHorizontal:10, paddingBottom:40, paddingTop:10}}>
                        <View style={{width:"100%", alignItems:"flex-end"}}>
                            <Text style={{fontSize:14, color:"#FFF", paddingVertical:10}}>0:39/1:00</Text>
                        </View>
                        <View style={{width:"100%", backgroundColor:"#ffffff", height:2}}>
                          <View style={{width:"60%", backgroundColor:"#fe2715", height:2}}></View>
                        </View>
                      </View>
                  </View>
                </View>
            </ImageBackground>
        </Animatable.View>
        <TouchableOpacity activeOpacity={0.5} style={{flex:1}} onPress={onClose}></TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({

})

export { UserVideoModal };

/* @flow weak */
import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome5';
const fontRegular = "Montserrat-Regular"

const DrawerItem = ({text,onPress,iconName}) => {
  if(onPress == null || onPress == ""){
    return (
      <TouchableOpacity activeOpacity={1}>
        <View style={styles.drawerItemContainer}>
          <View style={styles.drawerItemIconCont}>
            <Icon name={iconName} style={styles.drawerItemIcon} />
          </View>
          <Text style={styles.drawerItemText}>{text}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  else {
    return (
      <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
        <View style={styles.drawerItemContainer}>
          <View style={styles.drawerItemIconCont}>
            <Icon name={iconName} style={styles.drawerItemIcon} />
          </View>
          <Text style={styles.drawerItemText}>{text}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  drawerItemIconCont:{
    justifyContent:"center",
    alignItems:"center",
    width:50,
    height:35
  },
  drawerItemIcon:{
    fontSize:20,
    color:"#FFF"
  },
  drawerItemContainer:{
    flexDirection:"row",
    marginBottom:10,
    alignItems:"center",
    paddingHorizontal:10,
    paddingVertical:10
  },
  drawerItemText:{
    color:"#FFF",
    fontSize:18,
    paddingLeft:10,
    fontFamily:fontRegular
  }
})

export { DrawerItem };

/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ImageBackground,
  Image,
  TextInput
} from 'react-native';

import { Icon } from 'native-base';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
import * as Animatable from 'react-native-animatable';
import { Header } from '../common';

class TalentFilter extends Component {
  render(){
    const {filterAnimation, onClose} = this.props;
    return (
      <View style={{width:screenWidth, height:screenHeight, position:"absolute", zIndex:1000, backgroundColor: "rgba(0, 0, 0, 0.7)"}}>
        <Animatable.View
        animation={filterAnimation}
        style={{width:"100%", height:330}}
        duration={500}>
            <ImageBackground style={{width:"100%", height:330, position:"relative"}} resizeMode="cover" source={require("../../assets/talentMenuBg.png")}>
                <Header bgColor="transparent" headerText="Select Talent" onLeftPress={null} onRightPress={null} rightIcon={null} />
                <View style={{width:"100%", alignItems:"center", justifyContent:"center"}}>
                  <View style={{width:"80%", height:40, backgroundColor:"#FFF", borderRadius:30, paddingHorizontal:10}}>
                    <TouchableOpacity style={{flexDirection:"row"}} activeOpacity={1}>
                      <View style={{width:25, height:40, alignItems:"center", justifyContent:"center"}}>
                        <Icon type="FontAwesome" style={{fontSize:15, textAlign:"right", color:"#908f8f", fontWeight:"bold"}} name="search" />
                      </View>
                      <TextInput
                      style={{flex:1, paddingHorizontal:5, color:"#908f8f", fontSize:14, height:40}}
                      placeholderTextColor={"#908f8f"}
                      returnKeyType="search"
                      underlineColorAndroid="transparent"
                      onChangeText={(text) => this.searchText = text}
                      placeholder="Search" />
                    </TouchableOpacity>
                  </View>
                </View>
                <TouchableOpacity onPress={onClose} style={{width:50, height:50, position:"absolute", right:0, bottom:0}}></TouchableOpacity>
            </ImageBackground>
        </Animatable.View>
        <TouchableOpacity activeOpacity={0.5} style={{flex:1}} onPress={onClose}></TouchableOpacity>
      </View>
    )
  }
}

const styles = StyleSheet.create({

})

export { TalentFilter };

/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform
} from 'react-native';

import { MenuIcon, EditIcon } from '../common';
//import Icon from 'react-native-vector-icons';
import { Icon } from 'native-base';
const {width, height} = Dimensions.get("window");
const fontBold = "Montserrat-Regular"
const IconWidth = width/5
const SpacerWidth = width/10

class UserSubHeader extends Component {
  render(){
    const {profileOnPress, chatOnPress, notificationOnPress, walletOnPress, profileImage} = this.props;
    return (
      <View style={styles.subHeaderContainer}>
          <View style={{width: "100%", flexDirection: "row"}}>
            <View style={{width: SpacerWidth, opacity:0}}>
              <View style={{ width: SpacerWidth, height: 40, alignItems: "center" }}></View>
            </View>
            <TouchableOpacity onPress={profileOnPress}>
              <View style={{ 
                  width: IconWidth,
                  height: 40,
                  alignItems: "center",
                  justifyContent:"center",
                  borderRadius:50,
                  overflow:'hidden',
                  }}>
                <Image style={{ 
                  width: 35,
                  height: 35,
                  borderRadius:50,
                  borderWidth:1,
                  borderColor:"#FFF",

                 }} source={{uri:profileImage}} />
              </View>
            </TouchableOpacity>
            <View style={{width:0.7, height:15, backgroundColor:"#FFF", marginTop:15}}></View>
            <TouchableOpacity onPress={chatOnPress}>
              <View style={{ width: IconWidth, height: 40, alignItems: "center", justifyContent:"center" }}>
                <Image style={styles.subHeaderIcons} source={require("../../assets/chatIcon.png")} />
                <View style={[styles.bubble, {right:20, top:5}]}>
                    <Text style={styles.bubbleText}>1</Text>
                </View>
              </View>
            </TouchableOpacity>
            <View style={{width:0.7, height:15, backgroundColor:"#FFF", marginTop:15}}></View>
            <TouchableOpacity onPress={notificationOnPress}>
              <View style={{ width: IconWidth, height: 40, alignItems: "center", justifyContent:"center", position:"relative" }}>
                <Image style={styles.subHeaderIcons} source={require("../../assets/notificationIcon.png")} />
                <View style={[styles.bubble, {right:20, top:5}]}>
                    <Text style={styles.bubbleText}>2</Text>
                </View>
              </View>
            </TouchableOpacity>
            <View style={{width:0.7, height:15, backgroundColor:"#FFF", marginTop:15}}></View>
            <TouchableOpacity onPress={walletOnPress}>
              <View style={{ width: IconWidth, height: 40, alignItems: "center", justifyContent:"center", position:"relative" }}>
                <Image style={styles.subHeaderIcons} source={require("../../assets/walletIcon.png")} />
                <View style={[styles.bubble, {bottom:5, left:16}]}>
                    <Text style={styles.bubbleText}>4</Text>
                </View>
              </View>
            </TouchableOpacity>
            <View style={{width: SpacerWidth, opacity:0}}>
              <View style={{ width: SpacerWidth, height: 40, alignItems: "center" }}></View>
            </View>
          </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  subHeaderContainer:{
    width:width
  },
  subHeaderIcons:{
    width:IconWidth/2,
    height:40,
    resizeMode:"contain"
  },
  bubble:{
    backgroundColor:"red",
    width:14,
    height:14,
    borderRadius:7,
    position:"absolute"
  },
  bubbleText:{
    fontSize:10,
    color:"#FFF",
    textAlign:"center",
    fontWeight:"600"
  }
})

export { UserSubHeader };

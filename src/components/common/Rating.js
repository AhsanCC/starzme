/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  Image
} from 'react-native';

import {Icon} from 'native-base';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const fontRegular = "Montserrat-Regular"

class Rating extends Component {
  render(){
    const {rateReview} = this.props;
    if(rateReview == 0){
      return (
        <View style={{flexDirection:"row", marginVertical:5}}>
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
        </View>
      )
    }
    else if(rateReview == 1){
      return (
        <View style={{flexDirection:"row", marginVertical:5}}>
            <Text style={styles.rateText}>{rateReview}.0</Text>
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
        </View>
      )
    }
    else if(rateReview == 2){
      return (
        <View style={{flexDirection:"row", marginVertical:5}}>
            <Text style={styles.rateText}>{rateReview}.0</Text>
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
        </View>
      )
    }
    else if(rateReview == 3){
      return (
        <View style={{flexDirection:"row", marginVertical:5}}>
            <Text style={styles.rateText}>{rateReview}.0</Text>
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
        </View>
      )
    }
    else if(rateReview == 4){
      return (
        <View style={{flexDirection:"row", marginVertical:5}}>
            <Text style={styles.rateText}>{rateReview}.0</Text>
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmallInActive} name="star" />
        </View>
      )
    }
    else if(rateReview == 5){
      return (
        <View style={{flexDirection:"row", marginVertical:5}}>
            <Text style={styles.rateText}>{rateReview}.0</Text>
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
            <Icon style={styles.starIconsSmall} name="star" />
        </View>
      )
    }
  }
}

const styles = StyleSheet.create({
  starIconsSmall:{
    fontSize:12,
    marginHorizontal:1,
    color:"#f4b71e",
    lineHeight:15
  },
  starIconsSmallInActive:{
    fontSize:12,
    marginHorizontal:1,
    color:"#d3d4ed",
    lineHeight:15
  },
  rateText:{
    fontSize:12,
    fontFamily:fontRegular,
    paddingRight:3
  }
})

export { Rating };

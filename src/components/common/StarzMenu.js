/* @flow weak */
import React, {Component} from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Text,
  ImageBackground,
  Dimensions,
  Platform
} from 'react-native';

import { Icon } from 'native-base';
const {width, height} = Dimensions.get("window");
const fontBold = "Montserrat-Regular"

class StarzMenu extends Component {
  render(){
    const {boardPress,contestPress,newsPress,selected} = this.props;
    return (
      <View style={{width:"100%", flexDirection:"row", paddingHorizontal:5}}>
        <View style={[selected == '1' ? styles.isSelected : styles.notSelected, {flex:1, backgroundColor:"#8f48ee"}]}>
          <TouchableOpacity onPress={boardPress}>
            <Text style={{color:"#FFF", fontSize:15, fontWeight:"600", textAlign:"center", paddingVertical:10}}>Starz Board</Text>
          </TouchableOpacity>
        </View>
        <View style={[selected == '2' ? styles.isSelected : styles.notSelected, {flex:1, backgroundColor:"#f59502"}]}>
          <TouchableOpacity onPress={contestPress}>
            <Text style={{color:"#FFF", fontSize:15, fontWeight:"600", textAlign:"center", paddingVertical:10}}>Contest</Text>
          </TouchableOpacity>
        </View>
        <View style={[selected == '3' ? styles.isSelected : styles.notSelected, {flex:1, backgroundColor:"#fa4041"}]}>
          <TouchableOpacity onPress={newsPress}>
            <Text style={{color:"#FFF", fontSize:15, fontWeight:"600", textAlign:"center", paddingVertical:10}}>Starz News</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  isSelected:{
    width:width,
    shadowColor:"#528abd",
    shadowOffset:{width:0,height:0},
    shadowRadius:20,
    shadowOpacity:0.9,
    borderWidth:1,
    borderColor:"#00fffe",
    borderRadius:5
  },
  notSelected:{
    borderRadius:2
  }
})

export { StarzMenu };

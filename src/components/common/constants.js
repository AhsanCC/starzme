/* @flow weak */
import React from 'react';

// API urls
const Baseurl = "http://jemstech.net/StarzMe/WebApi/"

// Local images
const SplashBg = require('../../assets/SplashBg.png')
const MainBg = require('../../assets/bg.png')
const SplashLogo = require('../../assets/splashLogo.png')
const SplashLogoText = require('../../assets/splashLogoText.png')
const AppLogo = require('../../assets/logo.png')
const MainButton = require('../../assets/button.png')
const LoginUserIcon = require('../../assets/loginUserIcon.png')
const LoginPassIcon = require('../../assets/loginPassIcon.png')
const RegisterEmailIcon = require('../../assets/registerEmailIcon.png')
const RegisterGenderIcon = require('../../assets/registerGenderIcon.png')
const RegisterAgeIcon = require('../../assets/registerAgeIcon.png')
const RegisterCityIcon = require('../../assets/registerCityIcon.png')
const HeaderBg = require('../../assets/headerBg.png')
const DrawerBg = require('../../assets/drawerBg.png')
const Banner = require('../../assets/banner.png')
const CameraIcon = require('../../assets/cameraIcon.png')
const PaymentBg = require('../../assets/paymentBg.png')
const LocationIcon = require('../../assets/locationIcon.png')
const MapIcon = require('../../assets/mapIcon.png')
const CallIcon = require('../../assets/callIcon.png')
const GalleryIcon = require('../../assets/galleryIcon.png')
const SaveIcon = require('../../assets/saveIcon.png')
const SaveIconInActive = require('../../assets/saveIconInActive.png')
const MenuIcon = require('../../assets/menuIcon.png')
const LogoutIcon = require('../../assets/logout.png')
const NotificationIcon = require('../../assets/notificationIcon.png')
const FacebookIcon = require('../../assets/facebookIcon.png')
const TwitterIcon = require('../../assets/twitterIcon.png')
const InstaIcon = require('../../assets/instaIcon.png')
const LinkIcon = require('../../assets/linkIcon.png')
const GoogleIcon = require('../../assets/googleIcon.png')
const Leading = require('../../assets/leading.png')
const UploadVideoButton = require('../../assets/uploadVideoButton.png')

export {
  Baseurl,
  SplashBg,
  SplashLogo,
  SplashLogoText,
  MainBg,
  AppLogo,
  MainButton,
  LoginUserIcon,
  LoginPassIcon,
  RegisterEmailIcon,
  RegisterGenderIcon,
  HeaderBg,
  DrawerBg,
  Banner,
  CameraIcon,
  PaymentBg,
  LocationIcon,
  MapIcon,
  CallIcon,
  GalleryIcon,
  SaveIcon,
  SaveIconInActive,
  MenuIcon,
  LogoutIcon,
  RegisterAgeIcon,
  RegisterCityIcon,
  NotificationIcon,
  FacebookIcon,
  TwitterIcon,
  InstaIcon,
  LinkIcon,
  GoogleIcon,
  Leading,
  UploadVideoButton
};

/* @flow weak */
import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground
} from 'react-native';
import MasonryList from "react-native-masonry-list";

const MasonryListItem = ({items,onPress}) => {
  if(items.length == 1){
    var col = 1
  }
  else {
    var col = 3
  }
  return (
    <View style={{flex:1}}>
      <MasonryList
          images={items}
          spacing={1}
          columns={col}
          onPressImage={onPress}
          backgroundColor="#512c79"
      />
    </View>
  );
}

export {MasonryListItem};
